package com.seleniumreusable;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

public class SeleniumReusableMethods {


	WebDriver driver;

	public SeleniumReusableMethods(WebDriver driver) {
		this.driver = driver;
	}


	public void setText(By locator, String text) {


		try {
			WebElement locaterWebElement = driver.findElement(locator);
			Assert.assertTrue(locaterWebElement.isDisplayed());
			locaterWebElement.clear();
			locaterWebElement.sendKeys(text);
			Reporter.log("Entered at: " +locaterWebElement + " Text: " + text, true);


		}
		catch(Exception e) {
			Reporter.log("Exception raised in set Text Method: " + e.getMessage().toString());
		}
	}



	public void clickElement(By locator) {
		try {
			WebElement clickLocator = driver.findElement(locator);

			Assert.assertTrue(clickLocator.isDisplayed());
			clickLocator.click();
			Reporter.log("Clicked on Locator: " + clickLocator, true);

		}
		catch(Exception e) {
			Reporter.log("Exeption raised during click " + e.getMessage().toString());
		}
	}







}

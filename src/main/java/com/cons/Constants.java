package com.cons;

import java.io.File;

public class Constants {
	
	/*
	we will create constants for the below 
	
	
	filepaths 
	
	App title 
	
	App Error messages 
	
	
	constants means which are never changed and never reassigned*/ 
	
	
// in order to make them not to be reassigned we will use final keyword 
	
	
	public static final String CONFIG_PATH = System.getProperty("user.dir") + File.separator +
			"src" + File.separator +"main" + File.separator +"resources" + File.separator + "config.properties";
	
	public static final String TESTDATA_PATH = System.getProperty("user.dir") + File.separator +
			"src" + File.separator +"main" + File.separator +"resources" + File.separator + "testdata.json";
	
	
	
	
	
}

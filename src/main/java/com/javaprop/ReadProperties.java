package com.javaprop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Reporter;

import com.cons.Constants;

public class ReadProperties {
	
	
	
	
	public static String getProperty(String key) {
		
		
		System.out.println(Constants.CONFIG_PATH);
		Properties prop = new Properties();
		
		try {
			prop.load(new FileInputStream(new File(Constants.CONFIG_PATH)));
		} catch (IOException e) {
			Reporter.log("Exception raised in the method " + e.getMessage());
		}
		
		return prop.getProperty(key);
		
	}

}

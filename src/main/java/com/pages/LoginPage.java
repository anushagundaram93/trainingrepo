package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.javaprop.ReadProperties;
import com.seleniumreusable.SeleniumReusableMethods;

/**
 * @author rakesh
 * @version 1.0
 */

public class LoginPage {


	WebDriver driver;
	SeleniumReusableMethods sr;


	/**
	 * Login page constructor
	 * @author rakesh
	 * @version 1.0
	 * @param driver driver instance
	 */

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		sr = new SeleniumReusableMethods(driver);
	}



	//Locators
	By username = By.id("username");
	By password = By.xpath("//input[@type='password']");
	By login = By.xpath("//button[@type='submit']");
	By logout = By.xpath("//a[@href='/logout']//i[1]");


	//Methods 
	/**
	 * Login method which accepts valid username and valid password
	 * @author rakesh
	 * @version 1.0
	 * @param sUsername Valid username
	 * @param sPassword Valid Password
	 */	
	public void loginWithValidCredentials(String sUsername, String sPassword) {
		sr.setText(username, sUsername);

		sr.setText(password, sPassword);

		sr.clickElement(login);
	}


	/**
	 * Launch the login website
	 */
	public void launchLogin() {
		driver.get(ReadProperties.getProperty("url"));
		Reporter.log("Launched Login url: " + ReadProperties.getProperty("url"), true);
	}


	/**
	 * Valiate Login is Successful
	 * @return boolean 
	 */
	public boolean validateLoginSuccess() {
		return driver.findElement(logout).isDisplayed();
	}


}

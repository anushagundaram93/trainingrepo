package com.datadriven;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class DataDrivenTesting {

	/*	
	Data driven is testing with multiple sets of data 



	Reading the data from extenal source files like JSON, EXcel, csv

Everyone tend to use JSO

JSON

Java script object notation 
json-simple 
we need to parse the json in order to place the testdata in the scripts 

	 */



	public static String getJSONData(String key) {
		JSONParser parser = new JSONParser();

		Object obj = null;
		try {
			obj = parser.parse(new FileReader(new File(com.cons.Constants.TESTDATA_PATH)));
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		JSONObject jsonObj = (JSONObject)obj;


		return jsonObj.get(key).toString();


	}

}





package com.java;

public class StaticKeyword {
	/*
	
	
	Static keywords helps us in memory management 
	
	because once the static variables/methods are created they are bound to class 
	
	*/
	

	
	public static void main(String[] args) {
		
		/*
		StaticKeyword st1 = new StaticKeyword();
		st1.print();
		
		StaticKeyword st2 = new StaticKeyword();
		st2.print();
		*/
		
		StaticKeyword.print();
		
	}
	
	/*both the objects will use some memory 
	in order to eliminate the above memory issues we will use static so that we can call them directly through class without creating an object 
	
	*/

public static void print() {
	System.out.println("Print");
}




/*Disadvanvantages 

1. When we do parallel execution there is same values 

login( username1, password 1);

login(username2, password 2)*/

















}
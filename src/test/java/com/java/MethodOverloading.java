package com.java;

public class MethodOverloading {
	
	
	
	public static void main(String[] args) {
		
		
		MethodOverloading mo = new MethodOverloading();
		
		mo.sum();
		
		mo.sum(40, 100);
		
		mo.sum(890, 245346);
		
		
	}
	
	
	
	
	
	
	public void sum() {
		int a = 20, b = 40;
		
		System.out.println(a+b);
	}
	
	
	
	public void sum(int a, int b) {
		
		
		System.out.println(a +b);
	}
	

}

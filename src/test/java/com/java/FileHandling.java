package com.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileHandling {

	public static void main(String[] args) {
		

		
		
		/*1. creating a file - createNewFile
		
		2. Read a file content  - file input stream
		
		3. writing to a file  - file output stream*/
		
		
		
		File file = new File("D:\\Harish _Selenium_Practice\\Training\\src\\test\\resources\\rakeshFile.txt");
		
		
		try {
			if(file.createNewFile()) {
				System.out.println("File created");
			}
			else
				System.out.println("File already Exists");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		System.out.println("Can the file readable??" + file.canRead());
		
		
		System.out.println("Can the file is writable??" + file.canWrite());
		
		
		System.out.println("Path of file: " +file.getAbsolutePath());
		
		System.out.println(file.getPath());
		
		
		
		//write a content to the file 
		
		FileOutputStream fo = null;
		try {
			fo = new FileOutputStream(file.getPath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PrintWriter p = new PrintWriter(fo);
		p.println("My name is Rakesh");
		p.println("i teach Test Automation framework");
		
		p.close();
		
		
		FileInputStream fi =null;
		//Reading the contents of file 
		try {
			 fi = new FileInputStream(file.getPath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		@SuppressWarnings("resource")
		Scanner s = new Scanner(fi);
		
		
		while (s.hasNext()) {
			
			System.out.println(s.nextLine());
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}

package com.java;

import java.io.FileNotFoundException;

public class ExceptionHandling {

	
	
	/*main
	
	int i = 10;
	
	
	int j = 20; // Exception
	
	String str = "i am Rakesh";
	
	
	
	Exception will terminate the program 
	
	inorder to execute all the program steps we will do exception handling 
	
	
	try - catch block 
	
	
	
	whatever the code we feel that any exception might rise, we will place in try block 
	
	
	in the catch we will place the handling logic 
	
	
	int i = 10;
	
	try {
	int j = 20; // Exception 
	}
	catch(Exception e) {
		
	}
	
	String str = "i am Rakesh";
	
	
	
	Handling the Exceptions 
	
	
	
	
	There are two types 

	1. Exceptions - which can be handled
	
	2. Errors  - which cant be handled
	
	Usecase for Exception - if a file not found in the location and we are trying to access it 
	then it will throw filenotfound Exception 
	
	
	usecase for Errors - we have create lot of objects, variables in the memory , now you want to create an object/variable 
	
	you will be getting out of memory error 
	
	- Errors - Out of memory error , Assertion error , memory leak errors 
	
	
	
	
	
	
										Throwable 
										
										
										
						Exceptions 							Error  ( Out of memory error , Assertion error , memory leak error)
						
						Filenot found 
						
						SQL Exception 
						
						Arthemetic Exception 
						
						Array index out of bounds 
						
						Null pointer Exception
						
						
						*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		
		
		
		
		int i = 10;
		
		System.out.println("integer is: " + i);
		
		
		
		try {
		System.out.println(10/0);  // Exception
		}
		catch(ArithmeticException ae) {
			System.out.println(ae);
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage().toString());
		}
		
		
		String str = "I am Rakesh";
		
		
		System.out.println("String is: " + str);
	
		
/*		
		try -catch if its handled in the same class 
		
		
		throws - if the try catch handled in different class 
		
		
		
		throw - if you want to explicitly throw an exxception 
		*/
		
		
		
		try {
			throw new FileNotFoundException("File not Found");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	//Assignment - how to create user defined Exception 
	
	
	
	
	
	
	
	
	
	
						
						
						
						
						
						
						
						
						
						
	
	
	
	
	
	
	
	
	
}






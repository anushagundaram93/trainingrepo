package com.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Collections {
	
	
	/*
	1. List  --> Arraylist ( Dynamic Memory allocation )  --> duplicate values
	
	2. Map --> Credentials ( key, value) Hashmap
	
	
	3. set --> Hashset to eliminate the duplicate values */
	
	

	
	
	
	
	
	public static void main(String[] args) {
		
		
		
		List<String> a = new ArrayList<>();
		
		a.add("Rakesh"); //0
		a.add("is");  //1
		a.add("a"); //2
		
		a.add("Teacher");
		
		a.add("Teacher");
		a.add("Teacher");
		a.add("Teacher");
		a.add("Teacher");
		a.add("Teacher");
		a.add("Good");
		
		System.out.println("Size of list:" + a.size());
		
		
	/*	System.out.println(a.get(1));
		
		System.out.println(a.get(0));*/
		
		for(String b: a) {
			System.out.println(b);
		}
		
		Map<String, String> credentials = new HashMap<>();
		
		credentials.put("username1", "password1");
		credentials.put("username2", "password2");
		credentials.put("username3", "password3");
		credentials.put("username4", "password4");
		credentials.put("username5", "password5");
		
		
		System.out.println(credentials.get("username3"));
		
		
		Set<String> s= new HashSet<>();
		
		
		s.addAll(a);
		
		for(String c : s) {
			System.out.println(c);
		}
		
		
		System.out.println("Set sizeis:" + s.size());
		
		
	}
	
	
}

package com.java;

public class Arrays {
	
	
	
	public static void main(String[] args) {
		
		
	/*	
		Arrays will strore like typed data 
		
		
		int a = 10;
		
		if our problem store multiple data with same datattype we need to use Arrays 
		
		
		Syntax:- ArrayType[] arrayreference = new Arraytype[Size]
				
				Arrays are static memeory allocation - we need to define a size 
				
				
				
		
		int[] ainter = new int[3];
		
		
		if we are filling more than 3 elements we will get Array index out of bounds exception 
		
		
		
		Array index with 0
	Traditional	
		int[] ainter = new int[3];
		
		ainter[0] = 1;
		ainter[1] = 10;
		ainter[2] = 20;
		
	Modern
	
	
		String[] str = {"Rakesh", "Training", "selenium"};*/
	
		
		
		
		int[] a = {10, 6, 29};
		
		
		for(int i =0; i<=a.length-1; i++) {
			System.out.println(a[i]);
		}
		
		/*the above is a for loop which iterates based on condition
		
		if you want to iterate all the elements 
		
		without any condition then you can use enhanced for loop 
		*/
		
		
/*		for(data type reference : Array/list)
*/		
		for(int d : a) {
			System.out.println(d);
		}
		
	/**/
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}

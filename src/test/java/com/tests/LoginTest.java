package com.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.datadriven.DataDrivenTesting;
import com.driverfact.DriverFactory;
import com.pages.LoginPage;

import io.github.bonigarcia.wdm.WebDriverManager;


/**
 * This class contains Login related tests
 * @author rakesh
 */
public class LoginTest {
	WebDriver driver;

	@BeforeClass
	public void setup() {
		driver = DriverFactory.getDriver("chrome");
	}

	
	/**
	 * This Tests validates application with valid credentials 
	 */
	@Test
	public void testLoginWithValidCredentials() {


		LoginPage lp = new LoginPage(driver);

		lp.launchLogin();   

		lp.loginWithValidCredentials(DataDrivenTesting.getJSONData("username"), DataDrivenTesting.getJSONData("password"));

		Assert.assertTrue(lp.validateLoginSuccess(), "Login should be success");




	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}

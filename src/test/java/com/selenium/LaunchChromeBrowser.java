package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchChromeBrowser {
	
	public static void main(String[] args) {
			
		//setup the chromedriver using WebDriverManager
       WebDriverManager.chromedriver().setup();
 
        //Create driver object for Chrome
       WebDriver driver = new ChromeDriver();
 
        //Navigate to a URL
       driver.get("http://google.com");
 
        //quit the browser
       driver.quit();
       
       
       WebDriverManager.firefoxdriver().setup();
       FirefoxDriver d =new FirefoxDriver();
      //d.getTitle();
      d.get("http://google.com");
      driver.quit(); 
				
	}
	
	public void testLaunchChrome()
	{
		
	}

	
	public void testLaunchFirefox()
	{
		
	}
	
	
	
}

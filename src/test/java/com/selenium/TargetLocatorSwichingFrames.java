package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TargetLocatorSwichingFrames {



	/*
	Frames are having different html codes 



- developers will use a tag called as iframe 

-- we can identify frames by using right click on web page , if you are able to see reload frame then its a frame 



		driver.switchTo().frame( id or name or webelement);


		No such frame exception 


		Parent frames 
				child frames 



	in the above scenario we cant directly switch to child frame 

			--> switch to parent frame 

					--> then it will access to child frame 



	you need to come out of all frames 

			--> default content 



	 */		


	public static void main(String[] args) {

		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://the-internet.herokuapp.com/frames");	


		driver.findElement(By.linkText("Nes	ted Frames")).click();


		System.out.println(driver.getPageSource());


		System.out.println("====================================");


		driver.switchTo().frame("frame-top");


		System.out.println(driver.getPageSource());


		System.out.println("====================================");
		
		
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.name("frame-middle")));
		
		driver.switchTo().frame("frame-middle");
		
		System.out.println(driver.getPageSource());


		System.out.println("====================================");
		
		
		//switch back to parent frame 
		
		
		driver.switchTo().parentFrame();
		
		
		System.out.println(driver.getPageSource());


		System.out.println("====================================");
		
		
		driver.switchTo().defaultContent();
		
		System.out.println(driver.getPageSource());


		System.out.println("====================================");
		
		driver.quit();











	}



















}

package com.selenium;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FileDownload {

	
	public static void main(String[] args) {
	
		
	Map<String, Object> prefs = new HashMap<String, Object>();
	prefs.put("download.prompt_for_download", false);
	
		
	ChromeOptions co = new ChromeOptions();
	co.setExperimentalOption("prefs", prefs);
	
	
	//setup the chromedriver using WebDriverManager
    WebDriverManager.chromedriver().setup();

     //Create driver object for Chrome
    WebDriver driver = new ChromeDriver(co);

     //Navigate to a URL
    driver.get("https://chromedriver.storage.googleapis.com/index.html?path=84.0.4147.30/");
    
    
   new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.linkText("chromedriver_win32.zip"))).click();
    
	}
    
}

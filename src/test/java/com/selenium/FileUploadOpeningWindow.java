package com.selenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FileUploadOpeningWindow {

	
	
	public static void main(String[] args) throws AWTException {
		
	
	
	//setup the firefox using WebDriverManager
    WebDriverManager.firefoxdriver().setup();

     //Create driver object for firefox
    WebDriver driver = new FirefoxDriver();

     //Navigate to a URL
    driver.get("https://the-internet.herokuapp.com/upload");
    
    WebElement file = driver.findElement(By.id("file-upload"));
  //  driver.findElement(By.id("file-upload")).click();
    
    
   // file.click();
    
    JavascriptExecutor js = (JavascriptExecutor) driver;
    
    js.executeScript("arguments[0].click();", file);
    
    
    
    
    /*
    1. Create a filepath 
    
    2. Ctrl +c --> clipboard 
    
    3. ctrl+v --> paster 
    
    4. enter 
    */
    
    
    
    String filepath = "D:\\Harish _Selenium_Practice\\Training\\src\\test\\resources\\fileUpload.txt";
    
    
    
    //copy to clipboard 
    
    StringSelection s =new StringSelection(filepath);
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
    
    
    //Paste 
    /*in order to perform keyboard operations we need to use robot class 
    
    
    There are 2 methods 
    
    1. Key press - to press the key 
    
    2. key release - To release the key 
    
    
    Note:- Robot class is not from selenium 
    we are using Robot class of java awt package 
    
    */
    
    Robot r = new Robot();
    
    
    r.delay(2000);
    
    
    r.keyPress(KeyEvent.VK_CONTROL);
    r.keyPress(KeyEvent.VK_V);
    r.delay(2000);
    
    r.keyRelease(KeyEvent.VK_V);
    r.keyRelease(KeyEvent.VK_CONTROL);
    
    r.delay(2000);
    r.keyPress(KeyEvent.VK_ENTER);
    r.keyRelease(KeyEvent.VK_ENTER);
    
    r.delay(2000);
    
    
    driver.quit();
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	}
    
    
    
    
    
    
    
    
    
}

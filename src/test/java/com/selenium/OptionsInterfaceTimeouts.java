package com.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OptionsInterfaceTimeouts {

	public static void main(String[] args) {
		
	/*	
		1. page load timeout 
		
		2. script timeout 
		
		1
		
		2
		
		3*/
		

		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://the-internet.herokuapp.com/");
		
		
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		
		
		driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
		
		

	}

}

package com.selenium;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OptionsInterfaceWindow {


	public static void main(String[] args) throws InterruptedException {




		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://the-internet.herokuapp.com/");




		System.out.println("The browser resolution is: " + driver.manage().window().getSize());




		System.out.println("Height of the browser:" + driver.manage().window().getSize().getHeight());

		System.out.println("Width of the browser: " + driver.manage().window().getSize().getWidth());
		Thread.sleep(4000);

		//maximize the browser 


		driver.manage().window().maximize();


		// in selenium 4 we have minimize method 

		//driver.manage().window().minimize();


		Thread.sleep(4000);

		System.out.println("The browser resolution is: " + driver.manage().window().getSize());




		System.out.println("Height of the browser:" + driver.manage().window().getSize().getHeight());

		System.out.println("Width of the browser: " + driver.manage().window().getSize().getWidth());


		//X and y cordinates 

		System.out.println("Get position: " + driver.manage().window().getPosition());

		System.out.println("Get x: " + driver.manage().window().getPosition().getX() );
		System.out.println("Get y: " + driver.manage().window().getPosition().getY());



		Dimension d = new Dimension(800, 600);

		driver.manage().window().setSize(d);


		//driver.manage().window().fullscreen();

		driver.quit();
	}

}

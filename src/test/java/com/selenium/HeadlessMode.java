package com.selenium;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HeadlessMode {



	/*inorder to get script on headless mode we need to customize browser 

	to customize chrome - chrome options class,

	Firefox - firefox options 


	IE - InternetExplorerOptions 

	 */


	public static void main(String[] args) {

		
		ChromeOptions co = new ChromeOptions();
		//co.addArguments("--headless");
		//co.setHeadless(true);
		
		co.addArguments("--disable-notifications");
		co.addArguments("--disable-infobars");
		//co.addArguments("--incognito");
		co.addArguments("--start-maximized");
		co.addExtensions(new File(".\\src\\test\\resources\\SeleniumIDE.crx"));
		
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver(co);

		driver.get("https://the-internet.herokuapp.com/login");


		System.out.println(driver.getTitle());
		
		//driver.quit();

	}

}

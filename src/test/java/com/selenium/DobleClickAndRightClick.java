package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DobleClickAndRightClick {



	public static void main(String[] args) throws InterruptedException {


		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://www.facebook.com/");


		WebElement target = driver.findElement(By.xpath("//input[@data-testid='royal_login_button']"));



		Actions act = new Actions(driver);

		act.doubleClick(target).build().perform();

		
		Thread.sleep(5000);

		WebElement target2 = driver.findElement(By.id("email"));


		Actions act2 = new Actions(driver);

		act2.contextClick(target2).build().perform();


		driver.quit();




	}

}

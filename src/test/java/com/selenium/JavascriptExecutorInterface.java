package com.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class JavascriptExecutorInterface {
	
	
	public static void main(String[] args) throws InterruptedException {
		
		
		/*if you would like to emnbed javascript inside our java selenium program 
		
		we need to use javascript executor interface 
		*/
		
		/*Javascript executor has a method 
		
		1. executeAsynchronous script (Script, element( optional))
			
			sscript - javascript 
			
			element - webelement */
		
		
		/*1. Webelement which is hidden 
		
		2.  Refresh
		
		3. Scroll the windows 
		
		4. get Title 
		
		5. Navigate to a website 
		
		*/
		
		//setup the chromedriver using WebDriverManager
	       WebDriverManager.chromedriver().setup();
	 
	        //Create driver object for Chrome
	       WebDriver driver = new ChromeDriver();
	       
	       driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
		
		
		String alert = "alert('I am selenium trainer');";
		
		
		
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
	
		
		
		/*js.executeAsyncScript(alert);
		
		
		Thread.sleep(5000);
		
		Alert a = driver.switchTo().alert();
		
		a.accept();
		*/
		
		//js.executeAsyncScript("window.location = 'https://the-internet.herokuapp.com/login'");
		
		
		driver.get("https://www.facebook.com");
		
		
		Thread.sleep(5000);
		
		
		//String title = js.executeAsyncScript("return document.title;").toString();
		
		//System.out.println("Title is: " + title);
		
		
		driver.manage().window().maximize();
		
		//js.executeAsyncScript("window.scrollBy(0, 200)");
		
		
		/*WebElement signup = driver.findElement(By.xpath("//button[@type='submit']"));
		
		js.executeAsyncScript("arguments[0].click();", signup);*/
		
		
		js.executeAsyncScript("history.go(0)");
		driver.quit();
		
		
		
		
		
			
		
	}

}

package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebelementInterface {

	/*

	WebElement interface has the methods which interacts with the HTML web elements 

	Webelement is an interface 


	Remote webelement is the class implements all the methods of webelement 

	 */


	public static void main(String[] args) throws InterruptedException {


		//To launch a browser 
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		
		
		driver.get("https://www.facebook.com/");
		
	//	<input type="email" class="inputtext login_form_input_box" name="email" id="email" data-testid="royal_email">
		
		
		//Entering text
		
		driver.findElement(By.id("email")).sendKeys("Rakesh Email");
		
		
		//Get Attribute
		System.out.println("The Attribute type:" + driver.findElement(By.id("email")).getAttribute("type"));
		
		Thread.sleep(4000);
		//Clear 
		
		
		driver.findElement(By.id("email")).clear();
		
		
		
		System.out.println("Get the tagname of Email: " + driver.findElement(By.id("email")).getTagName());
		System.out.println("Email location:" +driver.findElement(By.id("email")).getLocation());
		
		System.out.println("Email Height and width:" +driver.findElement(By.id("email")).getSize());
		
		
		System.out.println("Email RECT:" +driver.findElement(By.id("email")).getRect());
		//Get the text on the website 
		
		System.out.println("The text displayed is: " + driver.findElement(By.xpath("//h2[text()='Connect with friends and the world around you on Facebook.']")).getText());
	
		System.out.println("Is signup displayed?? " + driver.findElement(By.name("websubmit")).isDisplayed());
		
		System.out.println("Is signup Enabled?? " + driver.findElement(By.name("websubmit")).isEnabled());		
		//Get the css property
		
		
		System.out.println("CSS value of background is: " + driver.findElement(By.name("websubmit")).getCssValue("background"));
		
		//For forms we can also submit method 
		
		driver.findElement(By.name("websubmit")).submit();
		
		
		//Click 
		
		driver.findElement(By.linkText("Forgot account?")).click();
		
		//Validation - isEnabled, is displayed, isselected
		
		/*isdisplayed - whenever if we need to know whether the element is displayed or not
		
					if the element displays it will return true otherwise  it will return false 
							
		isEnabled - if the elemnet is enabled it will return true otherwise false 
				
				
		isSelected - if the element is selected it will return true otherwise false 
				*/
		
		

	}











}

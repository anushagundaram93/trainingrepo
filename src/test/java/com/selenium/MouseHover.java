package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MouseHover {

	public static void main(String[] args) {
	
		//setup the chromedriver using WebDriverManager
		   WebDriverManager.chromedriver().setup();

		    //Create driver object for Chrome
		   WebDriver driver = new ChromeDriver();

		    //Navigate to a URL
		   driver.get("https://www.amazon.com/");
		   
		   
		   Actions act = new Actions(driver);
		   
		   act.moveToElement(driver.findElement(By.xpath("//span[text()='Hello, Sign in']"))).build().perform();
		
		   driver.quit();
		
		

	}

}

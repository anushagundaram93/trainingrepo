package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingDatePicker {


	public static void main(String[] args) {


		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://www.southwest.com/");


		WebElement depart = driver.findElement(By.xpath("(//span[text()='Depart Date']/following::input)[1]"));
		//WebElement depart = driver.findElement(By.xpath("(//input[@id='LandingAirBookingSearchForm_departureDate']"));
		
		depart.clear();

		depart.sendKeys("5/27");
		
		depart.sendKeys(Keys.TAB);
		
		
		WebElement returnXpath = driver.findElement(By.xpath("(//span[text()='Return date']/following::input)[1]"));
		returnXpath.clear();
		returnXpath.sendKeys("5/30");
		
		depart.sendKeys(Keys.TAB);
		
		//driver.quit();

		




	}

}

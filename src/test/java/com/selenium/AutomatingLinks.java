package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingLinks {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		 
        //Create driver object for Chrome
       WebDriver driver = new ChromeDriver();
 
        driver.get("https://the-internet.herokuapp.com/");
        //<a href="/abtest">A/B Testing</a>
        
        
     
        WebElement linkabtesting = driver.findElement(By.linkText("A/B Testing"));
        if(linkabtesting.isDisplayed()) {
        	System.out.println(linkabtesting.getAttribute("href"));
        	linkabtesting.click();
        }
        else {
        	System.out.println("Unable to display the link");
        }
        Thread.sleep(4000);
        System.out.println(driver.getCurrentUrl());
        driver.quit();
	}

}

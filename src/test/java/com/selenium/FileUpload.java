package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FileUpload {

	public static void main(String[] args) throws InterruptedException {
		/*there are 2 ways of uploading a file
		1.Without opening the window
		2.Opening the browse window.*/
		//<input id="file-upload" type="file" name="file">
	/*	There are 2 constraints to follow the fist approach
		1. Tag name shuld be input
		2. attribute type shuld be value-file*/
		
		
		
		//setup the firefox using WebDriverManager
	       WebDriverManager.firefoxdriver().setup();
	 
	        //Create driver object for firefox
	       WebDriver driver = new FirefoxDriver();
	 
	        //Navigate to a URL
	       driver.get("https://the-internet.herokuapp.com/upload");
	       
	       
	       Thread.sleep(5000);
	       
	       String filepath = "D:\\Harish _Selenium_Practice\\Training\\src\\test\\resources\\fileUpload.txt";
	       
	       
	       // We can use sendkeys and send the filepath so that automatically file will be uploaded 
	       
	       driver.findElement(By.id("file-upload")).sendKeys(filepath);
	       
	       
	       
	       
	       
	       
	       
		
		
		
		
		
		
		
		
	}

}

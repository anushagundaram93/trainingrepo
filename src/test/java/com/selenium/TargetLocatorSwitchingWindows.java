package com.selenium;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TargetLocatorSwitchingWindows {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		
		/*in order to switch between windows 
		
		getWindowsHandles - which will get all the window id's 
		
		--> store them in an array 
		--> switch to that window based on index 
		
		
		No such window exception */

		
		//setup the chromedriver using WebDriverManager
				WebDriverManager.chromedriver().setup();

				//Create driver object for Chrome
				WebDriver driver = new ChromeDriver();

				//Navigate to a URL
				driver.get("https://the-internet.herokuapp.com/windows");	
		
				driver.findElement(By.linkText("Click Here")).click();
				
				Thread.sleep(5000);
				
				Set<String> handles= driver.getWindowHandles();
				
				Object[] windows = handles.toArray();
				
				driver.switchTo().window((String) windows[1]);
				
				
				System.out.println(driver.getCurrentUrl());
				
				Thread.sleep(5000);
				
				driver.switchTo().window((String) windows[0]);
				
				System.out.println(driver.getCurrentUrl());
				
				Thread.sleep(5000);
				
				driver.quit();
				
				
				
				
				
				
				
				
				
				
				
				
				
		
		
		
		
	}

}

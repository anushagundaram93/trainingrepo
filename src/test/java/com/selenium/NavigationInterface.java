package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NavigationInterface {




	public static void main(String[] args) throws InterruptedException {

		//To launch a browser 
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();


		/*The difference between Navigate.to and get - 

		First thing both will do same operation 

		1. Navigate.to will not wait until the complete page loafds where as the get method will wait until all the page content loads 


		when i say html page loads 

			1. ALl html web elements 
			2. Javascript files 
			3. image files 
			4. links 


		2. Get will not store any history of URL however navigate will remem,ber the history */




		//To Navigate to the  the url 

		driver.navigate().to("http://google.com");


		//Navigate gain to a url 

		driver.navigate().to("http://facebook.com");

		//Since navigate interface remembers the browser history les play

		Thread.sleep(4000);
		driver.navigate().back();


		System.out.println(driver.getTitle());

		System.out.println("================================");
		Thread.sleep(4000);

		driver.navigate().forward();
		Thread.sleep(4000);

		System.out.println(driver.getTitle());

		System.out.println("================================");
		Thread.sleep(4000);

		driver.navigate().refresh();
		Thread.sleep(4000);
		System.out.println(driver.getTitle());

		System.out.println("================================");


		driver.quit();



















































	}

}

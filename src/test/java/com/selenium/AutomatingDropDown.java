package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingDropDown {

	public static void main(String[] args) throws InterruptedException {
	
	/*	Inorder to automate the dropdown we need to use select class of selenium.
		we have 3 diffrent methods to automate dropdown
		1.select by visible text
		2.select by index
		3.select by value
		Note:The Tag name should be select tag.
		Procedure:
			1.Find the dropdown elements.
			2. We need to make sure that the tag name is select tag.
			3.We need to use one of the above methods
			
			<select id="dropdown">
	    <option value="" disabled="disabled" selected="selected">Please select an option</option>
	    <option value="1">Option 1</option>
	    <option value="2">Option 2</option>
	  </select>*/
		
		WebDriverManager.chromedriver().setup();
		 
   
       WebDriver driver = new ChromeDriver();
      driver.get("https://the-internet.herokuapp.com/dropdown");
      
      WebElement drop = driver.findElement(By.id("dropdown"));
      
      Select dropdown= new Select(drop);   
       dropdown.selectByVisibleText("Option 2");
       Thread.sleep(4000);
       dropdown.selectByValue("1");
       Thread.sleep(4000);
       dropdown.selectByIndex(2);
       for (WebElement option : dropdown.getOptions()) {
		System.out.println(option.getText());
	}
       System.out.println(dropdown.getFirstSelectedOption().getText());
       //dropdown.deselectAll();
       driver.quit();
      
	}

}

package com.selenium;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DiffrentTypesOfWaits {

	public static void main(String[] args) {
		
		/*There are 2 types of waits
		1.Implicit Waits
		2.Explicit Waits
		
		Static Waits
		Thread.sleep(1000);
 THis is the wait which is there on java*/
		
		/*Dynamic wait - if the element is there it will go to the next steps 
		
		otherwise it wewill keep waiting for the element 
		
		
		Static wait- irrespective of element displayed or not it will wait for the time mentioned 
		*/
		
		
		
		/*Implicit wait will be applicable to all find element methods 
		
		
		Explicit wait is for a particular element 
		
		
		*/
		
		
		
		
		
		//setup the chromedriver using WebDriverManager
		   WebDriverManager.chromedriver().setup();

		    //Create driver object for Chrome
		   WebDriver driver = new ChromeDriver();

		    //Navigate to a URL
		   driver.get("https://www.facebook.com/");
		   
		   
		   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   
		   //<input type="email" class="inputtext login_form_input_box" name="email" id="email" data-testid="royal_email">
		   
		   
		   driver.findElement(By.id("email"));
		   
		   
		  // if you place the element and selenium is unable to find it on the web page you will be getting an Exception called as No such element Exception 
		   
		   // name
		   
		   driver.findElement(By.name("email"));
		   
		   
		   WebDriverWait wait = new WebDriverWait(driver, 20);
		   wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("login_form_input_box")));
		   
		   //classname
		   
		   driver.findElement(By.className("login_form_input_box"));
		   
		   
		   
		   
		   Wait wa = new FluentWait(driver)
				  .withTimeout(Duration.ofSeconds(2))
				  .pollingEvery(Duration.ofSeconds(2))
				  .ignoring(Exception.class);
				  
				  
				  
			
		   WebElement email = wait.until(new Function<WebDriver, WebElement>() {
			   public WebElement apply(WebDriver driver) {
				   return driver.findElement(By.className("login_form_input_box"));
			   }
			   
		});
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   driver.quit();
		
	}

}

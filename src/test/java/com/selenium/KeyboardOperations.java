	package com.selenium;
	
	import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;
	
	public class KeyboardOperations {
	
	
		public static void main(String[] args) {
	
			//setup the chromedriver using WebDriverManager
			WebDriverManager.chromedriver().setup();
	
			//Create driver object for Chrome
			WebDriver driver = new ChromeDriver();
	
			//Navigate to a URL
			driver.get("https://the-internet.herokuapp.com/key_presses");
			
			
			
			WebElement target = driver.findElement(By.cssSelector("input#target"));
			
			Actions act = new Actions(driver);
			
			
			act.sendKeys(Keys.ENTER).build().perform();
			
			
			act.moveToElement(target).click().keyDown(target, Keys.SHIFT).sendKeys("Key press").keyUp(target, Keys.SHIFT).build().perform();
			
			
			
			driver.quit();
	
			
			
	
		}
	}

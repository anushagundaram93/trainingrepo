package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingButtons {

	public static void main(String[] args) {
		//setup the chromedriver using WebDriverManager
	       WebDriverManager.chromedriver().setup();
	 
	        //Create driver object for Chrome
	       WebDriver driver = new ChromeDriver();
	 
	        //Navigate to a URL
	       driver.get("https://the-internet.herokuapp.com/login");
	       
	     //button[@type='submit']//i[1]
	       WebElement buttonLogin = driver.findElement(By.xpath("//button[@type='submit']"));
	       if(buttonLogin.isDisplayed()&& buttonLogin.isEnabled()) {
	    	   buttonLogin.click();
	       }
	       else {
	    	   System.out.println("Unable to Interact wth Login Button");
	       }

	}

}

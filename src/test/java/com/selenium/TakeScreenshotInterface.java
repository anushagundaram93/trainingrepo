package com.selenium;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TakeScreenshotInterface {


	static WebDriver driver;

	public static void main(String[] args) throws IOException {


		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();


		//Navigate to a URL
		driver.get("https://the-internet.herokuapp.com/login");

		TakeScreenshotInterface.TakeScreenshotOfPage("Launch page");

		driver.quit();

	}





	public static void TakeScreenshotOfPage(String name) throws IOException {

		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		File destFile = new File(".\\screenshots\\"+name + "_"+System.currentTimeMillis()+".jpeg");


		FileHandler.copy(srcFile, destFile);


	}

}

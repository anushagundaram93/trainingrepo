package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LocatorStrategies {
	
	
/*	in order to identify locators we need to use method findElement 
	
	1. FindELement is used to identify individual element 
	2. FindElements is used to identify multiple elements */
	
	/*There are 8 locator strategies 
	
	1. id
	2. name 
	3. classname 
	
	4. linkText
	5. partial link text 
	
	6. Xpath - index based techniques, contains text, starts with, ends-with, following, preceding, ancestor, descendent 
	7. CSS selector 
	8. Tagname*/
	
	
	
   
   
   /*
   lets go with ID locator strategy 
   
   
   1. ID 
   
   */
   
 
   
	public static void main(String[] args) {
		
		
		

		//setup the chromedriver using WebDriverManager
	   WebDriverManager.chromedriver().setup();

	    //Create driver object for Chrome
	   WebDriver driver = new ChromeDriver();

	    //Navigate to a URL
	   driver.get("https://www.facebook.com/");
	   
	   
	   //<input type="email" class="inputtext login_form_input_box" name="email" id="email" data-testid="royal_email">
	   
	   
	   driver.findElement(By.id("email"));
	   
	   
	  // if you place the element and selenium is unable to find it on the web page you will be getting an Exception called as No such element Exception 
	   
	   // name
	   
	   driver.findElement(By.name("email"));
	   
	   
	   //classname
	   
	   driver.findElement(By.className("login_form_input_box"));
	   
	   // xpath
	   
	   driver.findElement(By.xpath("(//label[text()='Password']/following::input)[1]"));
	   
	   
	   driver.findElement(By.xpath("//input[@id='email']"));
	   
	   
	   driver.findElement(By.xpath("//input[@data-testid='royal_email']"));
	   
	   
	   driver.findElement(By.cssSelector("#email"));
	   
	   
	   driver.findElement(By.linkText("Forgot account?"));
	   
	   driver.findElement(By.partialLinkText("Forgot"));
	   
	  System.out.println(driver.findElements(By.tagName("a")).size());
	   
	   
	   driver.quit();
	   
	   
	   
	   
	  /* 
	   // - root element 
	   /- child element 
	   */
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	}
	
	

}

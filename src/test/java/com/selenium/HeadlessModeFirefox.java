package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HeadlessModeFirefox {



	/*inorder to get script on headless mode we need to customize browser 

	to customize chrome - chrome options class,

	Firefox - firefox options 


	IE - InternetExplorerOptions 

	 */


	public static void main(String[] args) {

		
		FirefoxOptions fo = new FirefoxOptions();
		fo.setHeadless(true);
		
		
		
		
		
		
		//setup the chromedriver using WebDriverManager
		WebDriverManager.firefoxdriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new FirefoxDriver(fo);

		driver.get("https://the-internet.herokuapp.com/login");


		System.out.println(driver.getTitle());
		
		driver.quit();

	}

}

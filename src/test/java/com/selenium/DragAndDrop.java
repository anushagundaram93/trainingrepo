package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DragAndDrop {


	public static void main(String[] args) {

		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("http://www.dhtmlgoodies.com/scripts/drag-drop-custom/demo-drag-drop-3.html");
		
		
		WebElement drag = driver.findElement(By.id("box2"));
		
		WebElement drop = driver.findElement(By.xpath("(//div[@class='dragableBoxRight'])[3]"));
		
		
		
		
		Actions act = new Actions(driver);
		
		act.dragAndDrop(drag, drop).build().perform();
				
				
				
		WebElement source = driver.findElement(By.id("box7"));
		WebElement destination = driver.findElement(By.cssSelector("div#countries>div:nth-of-type(6)"));
		
		
		Actions act1 = new Actions(driver);
		act1.clickAndHold(source).moveToElement(destination).release().build().perform();
		
		
		
		driver.quit();
				
				
				
				
				
				
				
				
				
				
				
				
		
		
		
		
		
		
		

	}
}

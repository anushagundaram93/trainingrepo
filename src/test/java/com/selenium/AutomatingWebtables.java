package com.selenium;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingWebtables {
	
	
	/*webtables will be having HTML tag //table 
	
	A table tag will have 3 things 
	
	
	th - Tableheader 
	
	
	tr - Table row 
	
	
	td  - Table data
	
	
	
	if we need to get the data we need to play with the indexes of tr and td 
	*/
	
	
	public static void main(String[] args) {
		
		
		//setup the chromedriver using WebDriverManager
	       WebDriverManager.chromedriver().setup();
	 
	        //Create driver object for Chrome
	       WebDriver driver = new ChromeDriver();
	 
	        //Navigate to a URL
	       driver.get("https://the-internet.herokuapp.com/tables");
	       
	       
	       //Store the expected Table headers in a list 
	       
	       
	       List<String> expectedTableHeaders  = new ArrayList<>();
	       
	       
	       expectedTableHeaders.add("Last Name");
	       expectedTableHeaders.add("First Name");
	       expectedTableHeaders.add("Email");
	       expectedTableHeaders.add("Due");
	       expectedTableHeaders.add("Web Site");
	       expectedTableHeaders.add("Action");
	 
	       
	       String tableXpath = "//table[@id='table1']";
	       
	       WebElement table = driver.findElement(By.xpath(tableXpath));
	       
	       
	       String tableHeaderXpath = tableXpath + "/thead/tr/th";
	       
	       List<WebElement> tableHeaders = driver.findElements(By.xpath(tableHeaderXpath));
	       
	       System.out.println("Printing all headers");
	     
	       
	       for(int i =0; i < tableHeaders.size(); i ++) {
	    	   if(tableHeaders.get(i).getText().equals(expectedTableHeaders.get(i))) {
	    		   System.out.println(tableHeaders.get(i).getText() + "  Header Matched");
	    	   }
	       }
	       
	       System.out.println("Getting individual Header");
	       
	       System.out.println(driver.findElement(By.xpath("//table[@id='table1']/thead/tr/th[5]/span")).getText());
	       
	       
	       
	       
	       System.out.println(driver.findElement(By.xpath("//table[@id='table1']/tbody/tr[2]/td[2]")).getText());
	       
	       
	       driver.quit();
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
	       
		
		
	}
	
	

}

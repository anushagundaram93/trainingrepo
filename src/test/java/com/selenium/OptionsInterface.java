package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OptionsInterface {

	public static void main(String[] args) {
		
		
	/*	HTTP/Browser Cookies - 
		
		cookies will save those session information in simple note cookie will store some sensitive data */
		
		//setup the chromedriver using WebDriverManager
	       WebDriverManager.chromedriver().setup();
	 
	        //Create driver object for Chrome
	       WebDriver driver = new ChromeDriver();
	 
	        //Navigate to a URL
	       driver.get("https://the-internet.herokuapp.com/");
	       
	       
	       
	     System.out.println("Get all cookies: "  +driver.manage().getCookies());
	       
	       
	     System.out.println("Get session cookie: " +  driver.manage().getCookieNamed("rack.session"));
	     
	     
	  /*   Get session cookie: rack.session=BAh7CUkiD3Nlc3Npb25faWQGOgZFVEkiRWQ4NGMxNTk2NzUwY2YzMDY4NDRm%0AYjY3OTU2OGIyY2U3MDZkZGJmOTg4M2JiYmQ3ZWYyMTdkMmYzNDMyNWYyYzcG%0AOwBGSSIJY3NyZgY7AEZJIiVkMzY3NTUyMjJiMzJlMTU2NTNiYjgyYTExMzll%0ANDQ4NQY7AEZJIg10cmFja2luZwY7AEZ7B0kiFEhUVFBfVVNFUl9BR0VOVAY7%0AAFRJIi1iOWYxNTNjNjEwN2YwM2Q3MmVkYjhiZmI1YmVjYjJkY2U3OGNmNjE0%0ABjsARkkiGUhUVFBfQUNDRVBUX0xBTkdVQUdFBjsAVEkiLWQ0ZTdkMzNmNGVi%0ANzNiY2U4ZmY3MDdjODBiNjlhYzU3ZWU1YzlmYmMGOwBGSSIKZmxhc2gGOwBG%0AewA%3D%0A--c6981b55ae2500d719b179765a367093eeedb7d2; 
	     path=/; 
	     domain=the-internet.herokuapp.com*/
	     
	     //Delete all the cookies in our session 
	     
	     driver.manage().deleteAllCookies();
	     
	     
	     
	     
	     
	     
	     
	     

	     
	     
	       driver.quit();
		
		
		
	
		

	}

}

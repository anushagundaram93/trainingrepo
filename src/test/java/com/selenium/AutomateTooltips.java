package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomateTooltips {
	
/*	
	Tooltips are used to specify some information to the user 
	
	Tooltips are mostly there in the attribute called title */
	
	
	public static void main(String[] args) {
		
		//setup the chromedriver using WebDriverManager
	    WebDriverManager.chromedriver().setup();

	     //Create driver object for Chrome
	    WebDriver driver = new ChromeDriver();

	     //Navigate to a URL
	    driver.get("https://www.facebook.com/");
	    
	    
	    System.out.println(driver.getTitle());
	    
	    Actions a = new Actions(driver);
	    
	    WebElement tooltip = driver.findElement(By.xpath("//h1/a"));
	    
	    a.moveToElement(tooltip).build().perform();
	    
	    System.out.println(tooltip.getAttribute("title"));
	    
	    driver.quit();
		
		
	}

}

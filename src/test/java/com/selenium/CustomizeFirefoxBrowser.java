package com.selenium;

import java.util.logging.Level;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CustomizeFirefoxBrowser {



	/*inorder to get script on headless mode we need to customize browser 

	to customize chrome - chrome options class,

	Firefox - firefox options 


	IE - InternetExplorerOptions 

	 */


	public static void main(String[] args) {

		FirefoxProfile fp = new FirefoxProfile();
		
		
		fp.setPreference("browser.startup.homepage", "https://www.google.com");
		FirefoxOptions fo = new FirefoxOptions();
		//fo.setHeadless(true);
		fo.setAcceptInsecureCerts(true);
		fo.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, PageLoadStrategy.NORMAL);
		fo.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		fo.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		//fo.setCapability(CapabilityType.LOGGING_PREFS, new LoggingPreferences().enable(LogType.DRIVER, Level.INFO));
		
		
		//if the system does not know whete the firefox executable , we can explicitly give the path 
		//fo.setBinary("");
	
		
		fo.setProfile(fp);
		
		
		
		
		
		
		//setup the chromedriver using WebDriverManager
		WebDriverManager.firefoxdriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new FirefoxDriver(fo);

		

		System.out.println(driver.getTitle());
		
		driver.quit();

	}

}

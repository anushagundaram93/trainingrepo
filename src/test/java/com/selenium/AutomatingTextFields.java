package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AutomatingTextFields {

	public static void main(String[] args) {
		//setup the chromedriver using WebDriverManager
	       WebDriverManager.chromedriver().setup();
	 
	        //Create driver object for Chrome
	       WebDriver driver = new ChromeDriver();
	       
         driver.get("https://the-internet.herokuapp.com/login");
        // <input type="text" name="username" id="username">
         if(driver.findElement(By.id("username")).isDisplayed()) {
        	 
         
         
         driver.findElement(By.id("username")).clear();
         
         
         driver.findElement(By.id("username")).sendKeys("tomsmith");
         }
         else {
        	 System.out.println("Unable to  interact with User Name");
         }
        	 
        // <input type="password" name="password" id="password"> 
         WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
         if(inputPassword.isDisplayed()) {
                   
        	 inputPassword.clear();
                   
        	 inputPassword.sendKeys("SuperSecretPassword!");
             }
             else {
            	 System.out.println("Unable to  interact with Password");
             }
         driver.quit();
         
	}

}

package com.selenium;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TargetLocatorSwitchingAlerts {



	/*
	Alerts are 3 types 


	1. Simple alert  - Alert text along with ok button

	2. confirmation Alert -  Alert text along with ok and cancel button

	3. Prompt alert - Alert text having text fieds along with ok and cancel button*/
	/*

		procedure 

				1. We need to switch to alert 
				2. Ok --> ACCEPT 
				3. CANCEL - dismiss
				4. enter text --> sendKeys()
				5. Get the text - getText()


	Alert is the interface and remote alert is the class which implements the methods 


	if alert is not displayed within time we will get NosuchAlert exceptions
	 */			

	public static void main(String[] args) throws InterruptedException {


		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		WebDriver driver = new ChromeDriver();

		//Navigate to a URL
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");



		driver.findElement(By.xpath("//button[text()='Click for JS Alert']")).click();


		Thread.sleep(5000);

		Alert a = driver.switchTo().alert();
		System.out.println(a.getText());
		a.accept();
		
		
		
		driver.findElement(By.xpath("//button[text()='Click for JS Confirm']")).click();
		
		Thread.sleep(5000);
		Alert aConfirm = driver.switchTo().alert();
		
		System.out.println(aConfirm.getText());

		aConfirm.dismiss();
		
		
		driver.findElement(By.xpath("//button[text()='Click for JS Prompt']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.alertIsPresent());
		
		
		
		Thread.sleep(5000);
		Alert aPrompt = driver.switchTo().alert();
		
		System.out.println(aPrompt.getText());
		
		aPrompt.sendKeys("I am alert");
		
		
		aPrompt.accept();
		
		
		driver.quit();
		
		











	}













}

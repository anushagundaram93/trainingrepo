package com.driverfact;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {


	static WebDriver driver;

	public static WebDriver getDriver(String browser) {

		switch (browser) {
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			Reporter.log("Firefox browser choosen");
			break;

		case "chrome":
			WebDriverManager.chromedriver().setup();
			driver = new org.openqa.selenium.chrome.ChromeDriver();
			Reporter.log("Chrome browser choosen");
			break;

		default:
			Reporter.log("No browser choosen");
			break;
		}
		
		
		return driver;
	}
}

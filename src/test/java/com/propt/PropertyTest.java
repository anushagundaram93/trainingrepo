package com.propt;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.datadriven.DataDrivenTesting;
import com.javaprop.ReadProperties;


import io.github.bonigarcia.wdm.WebDriverManager;

public class PropertyTest {
	

	
	WebDriver driver;
	//ATUTestRecorder recorder;
	@BeforeClass
	public void setup() {
		
		
		//recorder = new ATUTestRecorder(".\\videos", this.getClass().getSimpleName().toString() + System.currentTimeMillis(), false);
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();
	}

	@Test
	public void testProperty()  {
	//	recorder.start();
		driver.get(ReadProperties.getProperty("url"));
		// <input type="text" name="username" id="username">
		if(driver.findElement(By.id("username")).isDisplayed()) {



			driver.findElement(By.id("username")).clear();

				Reporter.log("Username is: " + DataDrivenTesting.getJSONData("username"), true );
			driver.findElement(By.id("username")).sendKeys(DataDrivenTesting.getJSONData("username"));
		}
		else {
			System.out.println("Unable to  interact with User Name");
		}

		// <input type="password" name="password" id="password"> 
		WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
		if(inputPassword.isDisplayed()) {

			inputPassword.clear();
			Reporter.log("password is: " + DataDrivenTesting.getJSONData("password"), true );
			inputPassword.sendKeys(DataDrivenTesting.getJSONData("password"));
		}
		else {
			System.out.println("Unable to  interact with Password");
		}
	}

	@AfterClass
	public void teardown()  {
		//recorder.stop();
		driver.quit();
		
	}


}

package com.framework.video;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import io.github.bonigarcia.wdm.WebDriverManager;

public class VideoExecution {
	
	
	/*Video Execution is helpful to do RCA 
	
	We can record entire Test script execution 
	
	
	Eg"- ATU Test Recorder 

	
	
	1. We need to initialize ATU test recorder 
	
	it is a paramirized one having two options 
	
	1. Folder location 
	
	2. is Audio Enabled - true | false */
	
	/*ATU Test Recorder Can be dowloaded from the google drive link 
	
	https://drive.google.com/drive/u/0/folders/0B7rZvkq9tkwPRy1HLVJCdWtNekE
*/	
	WebDriver driver;
	//ATUTestRecorder recorder;
	@BeforeClass
	public void setup() {
		
		
		//recorder = new ATUTestRecorder(".\\videos", this.getClass().getSimpleName().toString(), false);
		//recorder = new ATUTestRecorder("C:\\Users\\haris\\Desktop\\videos\\recorder", false);
		
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();
	}

	@Test
	public void testVideo()  {
		//recorder.start();
		driver.get("https://the-internet.herokuapp.com/login");
		// <input type="text" name="username" id="username">
		if(driver.findElement(By.id("username")).isDisplayed()) {



			driver.findElement(By.id("username")).clear();


			driver.findElement(By.id("username")).sendKeys("tomsmith");
		}
		else {
			System.out.println("Unable to  interact with User Name");
		}

		// <input type="password" name="password" id="password"> 
		WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
		if(inputPassword.isDisplayed()) {

			inputPassword.clear();

			inputPassword.sendKeys("SuperSecretPassword!");
		}
		else {
			System.out.println("Unable to  interact with Password");
		}
	}

	@AfterClass
	public void teardown()  {
		//recorder.stop();
		driver.quit();
		
	}

	
}

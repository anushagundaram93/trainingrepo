package com.unit;

import org.testng.annotations.Test;

public class ExceptionHandlingTest {

	
	//in java we used to handle exceptions by using try-catch 
	
	
	@Test(expectedExceptions = ArithmeticException.class)
	public void testException() {
		System.out.println(1/0);
	}
}

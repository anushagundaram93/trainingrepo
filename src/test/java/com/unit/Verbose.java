package com.unit;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class Verbose {
	
	//Verbose is the int value where we can configure log of testng 
	
	@Test
	public void testVerbose() {
		Reporter.log("Verbose");
	}

}

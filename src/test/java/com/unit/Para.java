package com.unit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.java.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Para {
	
	
/*	
	i have a script now i would like to run on 2 browsers 
	
	I want to configure at suite level */
	
	
	WebDriver driver;
	
	
	@Parameters("browser")
	@Test
	public void browserLaunch(String browser) {
		
		switch (browser) {
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
					
			break;
			
		case "chrome":
			WebDriverManager.chromedriver().setup();
			driver = new org.openqa.selenium.chrome.ChromeDriver();
					
			break;

		default:
			Reporter.log("No browser choosen");
			break;
		}
		
		
		driver.get("http://www.google.com");
		
		
		driver.quit();
		
	}
	

}

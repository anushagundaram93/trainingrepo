package com.unit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SampleSeleniumTest {

	WebDriver driver;

	@BeforeClass
	public void setup() {
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();
	}

	@Test
	public void testInputFields() {
		driver.get("https://the-internet.herokuapp.com/login");
		// <input type="text" name="username" id="username">
		if(driver.findElement(By.id("username")).isDisplayed()) {



			driver.findElement(By.id("username")).clear();


			driver.findElement(By.id("username")).sendKeys("tomsmith");
		}
		else {
			System.out.println("Unable to  interact with User Name");
		}

		// <input type="password" name="password" id="password"> 
		WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
		if(inputPassword.isDisplayed()) {

			inputPassword.clear();

			inputPassword.sendKeys("SuperSecretPassword!");
		}
		else {
			System.out.println("Unable to  interact with Password");
		}
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}

}

package com.unit;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class EnableDisableTest {
	/*
	i have 3 testcases however there is an issue with 2nd testcases 
	now i want to disable the 2nd testcase during execution */
	
	
	@Test
	public void testFacebook() {
		Reporter.log("Facebook Testcase ", true);
	}
	

	@Test(enabled = false)
	public void testTwitter() {
		Reporter.log("Twitter testcase", true);
	}
	

	@Test
	public void testLinkedIn() {
		Reporter.log("Linkedin Testcase", true);
	}

}

package com.unit;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class DependencyTest {
	
	/*i have a testcase which performs login and logout 
	
	
	i would like to make sure if login passes then only i would like to execute logout 
	
	otherwise logout should be skipped */
	
	
	
	@Test
	public void testLogin() {
		Reporter.log("Login", true);
		Assert.fail();
	}
	
	@Test(dependsOnMethods = "testLogin")
	public void testLogout() {
		Reporter.log("Logout", true);
	}

}

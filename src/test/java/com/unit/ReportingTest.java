package com.unit;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class ReportingTest {
	
	
	/*TestNG provides reports 
	
	1. Index.html 
	
	2. Emailable report 
	
	3. Xml --> failed testscripts 
	*/
	@Test
	public void testReport() {
		Reporter.log("Test Report", true);
	}

}

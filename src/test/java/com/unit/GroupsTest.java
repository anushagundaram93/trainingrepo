package com.unit;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class GroupsTest {
	
/*	i want to have 2 groups 
	
	Eg:- sanity testcases, regression testcases 
	
	when i am performing sanity , i would like to execute sanity testcases 
	
	when i want to perform regression, i want to execute regression testcases */

	
	@Test(groups = {"sanity"})
	public void testScript1() {
		Reporter.log("Sanity", true);
	}
	
	@Test(groups = {"regression"})
	public void testscript2() {
		Reporter.log("Regression", true);
	}
	
	
	
}

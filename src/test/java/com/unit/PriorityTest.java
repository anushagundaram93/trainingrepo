package com.unit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.java.Webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PriorityTest {
	
	/*
	i have 3 testcases, i want to execute as per order which i suggest 
	
	--> Priority 
	
	we need to assign integers to it 
		
	o will be first priority 
	
	1 will execute the next*/
	
	WebDriver driver;
	
	@BeforeClass
	public void setup() {
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();
	}

	
	@Test(priority = -1)
	public void testFacebook() {
		Reporter.log("Facebook Testcase ", true);
		driver.get("https://www.facebook.com/");
	}
	

	@Test(priority = 1)
	public void testTwitter() {
		Reporter.log("Twitter testcase", true);
		driver.get("https://twitter.com/explore");
	}
	

	@Test(priority = 0)
	public void testLinkedIn() {
		Reporter.log("Linkedin Testcase", true);
		driver.get("https://ca.linkedin.com/");
	}
	
	@AfterClass
	public void teardown() {
		driver.quit();
	}

}

package com.unit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Soft {

	/*
	I have a registration testcase and i need to validate whether the registration is successful 

	Solution - Asssertions 


	There are two types of Assertions 


	1. Hard assertions - it will fail the script and terminate the program 

	2. Soft Assertions - it will not fail the assertion , it will complete the program execution and if there is any failure 
	in assertions it fails at the end 


	Hard Assert - Assert 

	Soft Assert - softAssert and assertAll method to validate all assertions */




	/*AssertEquals 

	Assert True 

	Assert false 
	 */


	WebDriver driver;

	@BeforeClass
	public void setup() {
		//setup the chromedriver using WebDriverManager
		WebDriverManager.chromedriver().setup();

		//Create driver object for Chrome
		driver = new ChromeDriver();
	}

	@Test
	public void testInputFields() {
		driver.get("https://the-internet.herokuapp.com/login");
		// <input type="text" name="username" id="username">


		SoftAssert sa = new SoftAssert();

		sa.assertFalse(driver.findElement(By.id("username")).isDisplayed(), "Username should not displayed");
		if(driver.findElement(By.id("username")).isDisplayed()) {



			driver.findElement(By.id("username")).clear();


			driver.findElement(By.id("username")).sendKeys("tomsmith");
		}
		else {
			System.out.println("Unable to  interact with User Name");
		}

		// <input type="password" name="password" id="password"> 
		WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
		if(inputPassword.isDisplayed()) {

			inputPassword.clear();

			inputPassword.sendKeys("SuperSecretPassword!");
		}
		else {
			System.out.println("Unable to  interact with Password");
		}

		driver.findElement(By.xpath("//button[@type='submit']//i[1]")).click();
		//<i class="icon-2x icon-signout"> Logout</i>

		sa.assertEquals(driver.findElement(By.xpath("//a[@href='/logout']//i[1]")).getText(), "Logout", "Logout should be displayed");

		sa.assertAll();

	}

	@AfterClass
	public void teardown() {

		driver.quit();
	}





}







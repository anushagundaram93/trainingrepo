package com.unit;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AnnotationsTest {
	
	
	/*
	Every Test will have few pre-requsites and postrequsites 
	
	Selenium can't execute them as per our order of execution 
	
	we can control the tests by using ANnotations 
	
	Tests which are part of before will execute before 
	
	Tests which are part of After will execute after 
	
	
	Before suite - will run before all the Test classes in the suite 
	
	After suite - After all the testscripts in the suite */
	
	
	
  @Test
  public void f() {
	Reporter.log("Actual Functionality", true);
  }
  
  
  @BeforeMethod
  public void beforeMethod() {
	  Reporter.log("Before each and every @Test the functionality", true);
  }

  @AfterMethod
  public void afterMethod() {
	  Reporter.log("After each and every @Test the functionality", true);
  }
  

  @BeforeClass
  public void beforeClass() {
	  Reporter.log("Before class", true);
  }

  @AfterClass
  public void afterClass() {
	  Reporter.log("After class", true);
  }

  @BeforeTest
  public void beforeTest() {
	  
	  Reporter.log("Before the Test", true);
  }

  @AfterTest
  public void afterTest() {
	  Reporter.log("After Test", true);
  }

  @BeforeSuite
  public void beforeSuite() {
	  
	  Reporter.log("Before all suite", true);
  }

  @AfterSuite
  public void afterSuite() {
	  Reporter.log("After all suite", true);
	  
	  
  }

}

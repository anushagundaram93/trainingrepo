package com.unit;

import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Test;

public class SkipTestcases {

/*	
	
	i have 3 testcases however one of the testcase functionality is not working fine 
	
	i would like to skip the testcase 
	
	
	solution - throw skip test executuion*/
	
	
	
	
	
	
	@Test
	public void testFacebook() {
		Reporter.log("Facebook Testcase ", true);
	}
	

	@Test()
	public void testTwitter() {
		throw new SkipException("Twitter functionality is not working so skipping the test");
	}
	

	@Test
	public void testLinkedIn() {
		Reporter.log("Linkedin Testcase", true);
	}
}
